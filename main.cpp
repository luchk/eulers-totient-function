#include <iostream>

unsigned long long int euler(unsigned long long int n) 
{
	unsigned long long int ret = 1;
	
	for(int i = 2; i * i <= n; ++i)
	{
		unsigned long long int p = 1;
		
		while(n % i == 0)
		{
			p *= i;
			n /= i;
		}
		if((p /= i) >= 1)
		{
			ret *= p * (i - 1);
		}
	}
	return --n ? n * ret : ret;
}
int main()
{
	std::cout<< euler(124325436);

	return 0;
}
